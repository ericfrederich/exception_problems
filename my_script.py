#!/usr/bin/env python

import acmebindings

def do_something():
    acmebindings.trigger_action("CUSTOM_handler")

def main():
    do_something()

if __name__ == '__main__':
    acmebindings.init_acme()
    main()
