all: application
	
application: dependencies application.c
	gcc -I./acme -L./acme application.c -lacme -ldl -o application -lpython2.6
runc: application
	@echo -e "\n\nchecking application...\n"
	@LD_LIBRARY_PATH=./acme CUSTOMIZATION_LIBRARY=customizations/libcust.so PYTHONPATH=./customizations ./application
runpy: application
	@echo -e "\n\nrunning python script...\n"
	@LD_LIBRARY_PATH=./acme CUSTOMIZATION_LIBRARY=customizations/libcust.so PYTHONPATH=./customizations ./my_script.py 

clean:
	rm -f application
	$(MAKE) -C acme clean
	$(MAKE) -C acmebindings clean
	$(MAKE) -C customizations clean

dependencies:
	./setup_env.sh
	$(MAKE) -C acme
	$(MAKE) -C acmebindings
	$(MAKE) -C customizations
