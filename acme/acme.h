#ifndef __ACME_H_INCLUDE
#define __ACME_H_INCLUDE

int init_acme(void);
int trigger_action(char* name);
int set_value_strings(char* name, int val);

#endif