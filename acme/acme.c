#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

void* CUSTOMIZATIONS=NULL;

int init_acme(void){
    char* fname = getenv("CUSTOMIZATION_LIBRARY");
    if(fname != NULL){
        printf("Registering customizations from %s\n", fname);
        CUSTOMIZATIONS = dlopen(fname, RTLD_LAZY);
        if(CUSTOMIZATIONS != NULL){
            printf("...success\n");
            return 0;
        } else {
            printf("...fail\n");
        }
    }
    return 1;
}

int dynamic_call_str_int(char* fn_name, char* name, int val){
    if(CUSTOMIZATIONS != NULL){
        int (*fn_ptr)(char* name, int val) = dlsym(CUSTOMIZATIONS, fn_name);
        if(fn_ptr != NULL){
            return fn_ptr(name, val);
        }
    }
    return -1;
}

int trigger_action(char* name){
    int ret;
    printf("libacme: trigger_action %s\n", name);
    if(CUSTOMIZATIONS != NULL){
        int (*fn_ptr)(void) = dlsym(CUSTOMIZATIONS, name);
        if(fn_ptr != NULL){
            ret = fn_ptr();
            printf("libacme: got %d from %s\n", ret, name);
            return ret;
        }
        printf("libacme: %s not found in customizations\n", name);
        return -1;
    }
    printf("libacme: no customizations found\n");
    return -1;
}

int set_value_strings(char* name, int val){
    if(strcmp(name, "XYZ") == 0){
        printf("libcame: calling CUSTOM_set_property instead of setting value\n", name, val);
        return dynamic_call_str_int("CUSTOM_set_property", name, val);
    }
    printf("libcame: setting %s to %d\n", name, val);
    return 0;
}
