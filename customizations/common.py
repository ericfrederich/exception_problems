from acmebindings import AcmeException
from functools import wraps
import traceback

def EXCEPTION_TO_RETCODE(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            ret = func(*args, **kwargs)
        except AcmeException, err:
            return err.num
        except Exception, e:
            print traceback.format_exc()
            return 789
        return ret
    return wrapped
