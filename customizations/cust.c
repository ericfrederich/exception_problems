#include <Python.h>
#include <stdlib.h>
#include <stdio.h>

#define ACME_ok 0

int S4_run_python_script(const char* module, const char* function, PyObject* pArgs, PyObject** result)
{
    Py_Initialize();
    int retcode = ACME_ok;
    PyObject *pName, *pModule, *pFunc;
    int keep_result = 1;
    PyObject* dummy_result;
    
    if(result == NULL) {
        result = &dummy_result;
        keep_result = 0;
    } else {
        keep_result = 1;
    }
    
    pName = PyString_FromString(module);
    // Error checking of pName left out
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    
    if (pModule != NULL) {
        PyImport_ReloadModule(pModule);
        pFunc = PyObject_GetAttrString(pModule, function);
        // pFunc is a new reference
        
        if (pFunc && PyCallable_Check(pFunc)) {
            *result = PyObject_CallObject(pFunc, pArgs);
            Py_XDECREF(pArgs);
            if (*result != NULL) {
                if(PyTuple_Check(*result)) {
                    retcode = PyInt_AsLong(PyTuple_GetItem(*result, 0));
                } else {
                    retcode = PyInt_AsLong(*result);
                }
                if(retcode == ACME_ok) {
                    printf("Result of call to %s:%s: ACME_ok\n", module, function);
                } else {
                    printf("Result of call to %s:%s: %d\n", module, function, retcode);
                }
                // if the caller is keeping the result they are responsible for DECREF'ing it
                if(!keep_result) {
                    Py_DECREF(*result);
                }
            }
            else {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                printf("ERROR: Call failed %s:%s\n", module, function);
                retcode = 919002;
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            printf("Cannot find function \"%s\"\n", function);
            retcode = 919002;
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        printf("Failed to load \"%s\"\n", module);
        retcode = 919002;
    }
    //printf("final ref count from S4_run_python_script %ld\n", (*result)->ob_refcnt);
    return retcode;
}

int CUSTOM_handler(void){
    int ret;
    printf("enter CUSTOM_handler from the C side\n");
    ret = S4_run_python_script("handler", "handler", NULL, NULL);
    printf("exit CUSTOM_handler from the C side\n");
    return ret;
}

int CUSTOM_set_property(char* name, int value){
    int ret;
    printf("enter CUSTOM_set_property from the C side\n");
    PyObject* pArgs = Py_BuildValue("si", name, value);
    ret = S4_run_python_script("set_property", "set_property", pArgs, NULL);
    printf("exit CUSTOM_set_property from the C side\n");
    return ret;
}

int spam_pre(int x){
    printf("pre  spam customization\n");
}
int spam_post(int x){
    printf("post spam customization\n");
}

int eggs_pre(int x){
    printf("pre  eggs customization\n");
}
int eggs_post(int x){
    printf("post eggs customization\n");
}


