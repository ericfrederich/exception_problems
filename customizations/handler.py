import acmebindings
from common import EXCEPTION_TO_RETCODE

@EXCEPTION_TO_RETCODE
def handler():
    acmebindings.set_value_strings("XYZ", 234)
    return 0
