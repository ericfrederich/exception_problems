import acmebindings
from common import EXCEPTION_TO_RETCODE

@EXCEPTION_TO_RETCODE
def set_property(name, val):
    print 'inside Python set_property with', name, 'and', val
    if val == 234:
        raise acmebindings.AcmeException(999)
    return 0
