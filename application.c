#include <stdio.h>
#include <acme.h>

#define ACME_ok 0

#define CALL_ACME( function ) \
{ \
    int ifail = ACME_ok;                                                            \
    ifail = function;                                                               \
    if ( ifail != ACME_ok )                                                         \
    {                                                                               \
        fprintf(stderr, "======== ITK ERROR %s:%d ========\n", __FILE__, __LINE__); \
        fprintf(stderr, "  Call      - %s\n"   , #function );                       \
        fprintf(stderr, "  Return    - %d\n"   , ifail );                           \
        fflush (stderr);                                                            \
        retcode = ifail;                                                            \
        goto cleanup;                                                               \
    }                                                                               \
}


int main(int argc, char* argv[]){
    int retcode = ACME_ok;
    init_acme();
    CALL_ACME(trigger_action("CUSTOM_handler"));
cleanup:
    return retcode;
}
