#!/bin/bash

if [ ! -d venv ]; then
    echo "no venv directory... creating it"
    virtualenv venv
    source venv/bin/activate
    
    # patch Pillow to install on RHEL6
    pip install -d . pillow
    TARFILE=`ls Pillow*.tar.gz`
    PILLOWDIR=${TARFILE%.tar.gz}
    tar -xvf $TARFILE
    # remove this line from setup.py
    grep -v '_add_directory(library_dirs, "/usr/lib")' $PILLOWDIR/setup.py > $PILLOWDIR/setup.py.new
    mv $PILLOWDIR/setup.py.new $PILLOWDIR/setup.py
    pip install $PWD/$PILLOWDIR
    
    rm -rf $PILLOWDIR $TARFILE
    
    pip install seqdiag
    
    pip install cython
    
    echo -e "\n\n\nall done; run the following in your terminal..."
    echo "source venv/bin/activate"
fi
