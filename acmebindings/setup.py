from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [
    Extension("acmebindings", ["acmebindings.pyx"],
              libraries=["acme"],
              include_dirs=['../acme/'],
              library_dirs=['../acme/']
              ),

    ]

)
