cimport cacmelib

class AcmeException(Exception):
    def __init__(self, num):
        
        import traceback
        print '!' * 80, "AcmeException CREATED"
        print '\n'.join(traceback.format_stack())
        
        super(AcmeException, self).__init__()
        self.num = num

def non_zero_to_exception(retcode):
    if retcode != 0:
        raise AcmeException(retcode)
    return retcode

#
# wrappers for the C functions
#
def init_acme():
    print 'calling init_acme from binding'
    return non_zero_to_exception(cacmelib.init_acme())

def trigger_action(name):
    print 'calling trigger_action(%s) from binding' % repr(name)
    return non_zero_to_exception(cacmelib.trigger_action(name))

def set_value_strings(name, val):
    print 'calling set_value_strings from binding'
    return non_zero_to_exception(cacmelib.set_value_strings(name, val))
