cdef extern from "acme.h":
    
    int init_acme()
    int trigger_action(char* name)
    int set_value_strings(char* name, int val)
